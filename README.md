A Todo-Backend using Rocket and Rust
===

* What's this? See http://todobackend.com/.
* [Run the Todo-Backend Specs](https://www.todobackend.com/specs/index.html?https://todo-backend-rocket-rust.herokuapp.com/) against this application
* [How to Deploy a Rocket Application to Heroku](http://www.duelinmarkers.com/2017/10/21/how-to-deploy-a-rocket-application-to-heroku.html)
